/*****************************************************************************
 * fs.c: file system access plugin
 *****************************************************************************
 * Copyright (C) 2001-2006 VLC authors and VideoLAN
 * Copyright © 2006-2007 Rémi Denis-Courmont
 *
 * Authors: Christophe Massiot <massiot@via.ecp.fr>
 *          Rémi Denis-Courmont <rem # videolan # org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_access.h>
#include <vlc_demux.h>
#include <vlc_charset.h>
#include <vlc_input.h>
#include <vlc_access.h>
#include <vlc_dialog.h>
#include <vlc_fs.h>
#include <vlc_url.h>

#include "memfile.h"

static const char *const psz_recursive_list[] = { "none", "collapse", "expand" };
static const char *const psz_recursive_list_text[] = {
    N_("none"), N_("collapse"), N_("expand") };

vlc_module_begin ()
    set_description( N_("MemFile input") )
    set_shortname( N_("MemFile") )
    set_category( CAT_INPUT )
    set_subcategory( SUBCAT_INPUT_ACCESS )
    set_capability( "access", 50 )
    add_shortcut( "memfile")
    set_callbacks( MemFileOpen, MemFileClose )
vlc_module_end ()

typedef int (*memfile_open_t)();
typedef int (*memfile_size_t)(uint64_t * size);
typedef int (*memfile_close_t)();
typedef int (*memfile_seek_t)(uint64_t seek);
typedef int (*memfile_read_t)(char * buf, int size);

static int MemFileControl( access_t *p_access, int i_query, va_list args );
static ssize_t MemFileRead (access_t *p_access, uint8_t *p_buffer, size_t i_len);
static int MemFileSeek (access_t *p_access, uint64_t i_pos);

typedef struct {

  memfile_open_t open;
  memfile_size_t size;
  memfile_close_t close;
  memfile_seek_t seek;
  memfile_read_t read;
  
} memfile_sys_t;

/*****************************************************************************
 * MemFileOpen: open the file
 *****************************************************************************/
int MemFileOpen( vlc_object_t *p_this )
{
    access_t     *p_access = (access_t*)p_this;

    memfile_sys_t *sys = calloc(1, sizeof(*sys));
    if (!sys)
        return VLC_ENOMEM;

    if (!strcasecmp (p_access->psz_access, "memfile"))
    {
        char *end;
       
        end = p_access->psz_location;
        
        if (*end == '\0') {
            msg_Err (p_access, "need more arguments 1");
            return VLC_EGENERIC;
        } else {
            sys->open = (memfile_open_t)(intptr_t) strtoll (end, &end, 10);
        }

        if (*end == '\0') {
            msg_Err (p_access, "need more arguments 2");
            return VLC_EGENERIC;
        } else if (*end == '/')  {
            sys->close = (intptr_t) strtoll (++end, &end, 10);
        }

        if (*end == '\0') {
            msg_Err (p_access, "need more arguments 3");
            return VLC_EGENERIC;
        } else if (*end == '/')  {
            sys->size = (intptr_t) strtoll (++end, &end, 10);
        }

        if (*end == '\0') {
            msg_Err (p_access, "need more arguments 4");
            return VLC_EGENERIC;
        } else if (*end == '/')  {
            sys->seek = (intptr_t) strtoll (++end, &end, 10);
        }

        if (*end == '\0') {
            msg_Err (p_access, "need more arguments 5");
            return VLC_EGENERIC;
        } else if (*end == '/') {
            sys->read = (intptr_t) strtoll (++end, &end, 10);
        }
        
        if (*end != '\0') {
            msg_Err (p_access, "too much arguments %s", end);
            return VLC_EGENERIC;
        }
        
        if(!sys->close){
            msg_Err (p_access, "bad close argument");
            return VLC_EGENERIC;
        }
        if(!sys->open){
            msg_Err (p_access, "bad open argument");
            return VLC_EGENERIC;
        }
        if(!sys->size){
            msg_Err (p_access, "bad size argument");
            return VLC_EGENERIC;
        }
        if(!sys->read){
            msg_Err (p_access, "bad read argument");
            return VLC_EGENERIC;
        }
        if(!sys->seek){
            msg_Err (p_access, "bad seek argument");
            return VLC_EGENERIC;
        }
    }
    else
    {
        msg_Err (p_access, "need more arguments");
        return VLC_EGENERIC;
    }

    if(sys->open() != VLC_SUCCESS)
      return VLC_EGENERIC;

    access_InitFields (p_access);
    p_access->pf_block = NULL;
    p_access->pf_control = MemFileControl;
    p_access->p_sys = (access_sys_t*)sys;

    p_access->pf_read = MemFileRead;
    p_access->pf_seek = MemFileSeek;
    sys->size(&p_access->info.i_size);

    return VLC_SUCCESS;
}

/*****************************************************************************
 * FileClose: close the target
 *****************************************************************************/
void MemFileClose (vlc_object_t * p_this)
{
    access_t     *p_access = (access_t*)p_this;
    memfile_sys_t * p_sys = p_access->p_sys;

    if (p_sys->close != NULL)
    {
      p_sys->close();
    }

    free (p_sys);
}


#include <vlc_network.h>

/**
 * Reads from a regular file.
 */
static ssize_t MemFileRead (access_t *p_access, uint8_t *p_buffer, size_t i_len)
{
    memfile_sys_t * p_sys = p_access->p_sys;

    ssize_t val = p_sys->read(p_buffer, i_len);
    
    if (val < 0)
    {
        msg_Err (p_access, "read error: %m");
        return -1;
    }

    p_access->info.i_pos += val;
    p_access->info.b_eof = !val;

    uint64_t size;
    p_sys->size(&size);

    if (p_access->info.i_size != size) {
        p_access->info.i_size = size;
        p_access->info.i_update |= INPUT_UPDATE_SIZE;
    }

    return val;
}


/*****************************************************************************
 * Seek: seek to a specific location in a file
 *****************************************************************************/
static int MemFileSeek (access_t *p_access, uint64_t i_pos)
{
    memfile_sys_t * p_sys = p_access->p_sys;
    
    p_access->info.i_pos = i_pos;
    p_access->info.b_eof = false;

    return p_sys->seek(i_pos);
}

/*****************************************************************************
 * Control:
 *****************************************************************************/
static int MemFileControl( access_t *p_access, int i_query, va_list args )
{
    memfile_sys_t * p_sys = p_access->p_sys;
    
    bool    *pb_bool;
    int64_t *pi_64;

    switch( i_query )
    {
        /* */
        case ACCESS_CAN_SEEK:
        case ACCESS_CAN_FASTSEEK:
            pb_bool = (bool*)va_arg( args, bool* );
            *pb_bool = (p_access->info.i_size != -1);
            break;

        case ACCESS_CAN_PAUSE:
        case ACCESS_CAN_CONTROL_PACE:
            pb_bool = (bool*)va_arg( args, bool* );
            *pb_bool = p_access->info.i_size != -1;
            break;

        /* */
        case ACCESS_GET_PTS_DELAY:
            pi_64 = (int64_t*)va_arg( args, int64_t * );
            *pi_64 = var_InheritInteger (p_access, "file-caching"); // network-caching
            *pi_64 *= 1000;
            break;

        /* */
        case ACCESS_SET_PAUSE_STATE:
            /* Nothing to do */
            break;

        case ACCESS_GET_TITLE_INFO:
        case ACCESS_SET_TITLE:
        case ACCESS_SET_SEEKPOINT:
        case ACCESS_SET_PRIVATE_ID_STATE:
        case ACCESS_GET_META:
        case ACCESS_GET_PRIVATE_ID_STATE:
        case ACCESS_GET_CONTENT_TYPE:
            return VLC_EGENERIC;

        default:
            msg_Warn( p_access, "unimplemented query %d in control", i_query );
            return VLC_EGENERIC;

    }
    return VLC_SUCCESS;
}
